package com.easipro.payrole.business.empbenefits;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
@Service
public class EmployeeBenefitServiceImpl implements EmployeeBenefitService {

    private final EmployeeBenefitRepository employeeBenefitRepository;


    @Override
    public Optional<EmployeeBenefit> get(Object id) {
        return employeeBenefitRepository.findById((Long) id);
    }

    @Override
    public List<EmployeeBenefit> getAll() {
        return employeeBenefitRepository.findAll();
    }

    @Override
    public EmployeeBenefit save(EmployeeBenefit employeeBenefit) {
        return employeeBenefitRepository.save(employeeBenefit);
    }

    @Override
    public void delete(EmployeeBenefit employeeBenefit) {
        employeeBenefitRepository.delete(employeeBenefit);

    }
}
