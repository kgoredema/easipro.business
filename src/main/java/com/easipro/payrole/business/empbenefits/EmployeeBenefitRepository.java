package com.easipro.payrole.business.empbenefits;

import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployeeBenefitRepository extends JpaRepository<EmployeeBenefit, Long> {

}
