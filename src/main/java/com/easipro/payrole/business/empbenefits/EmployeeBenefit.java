package com.easipro.payrole.business.empbenefits;



import com.easipro.payrole.business.benefit.Benefit;
import com.easipro.payrole.business.common.BaseEntity;
import com.easipro.payrole.business.employee.Employee;
import java.math.BigInteger;
import lombok.*;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Getter
@Setter
@Builder
@ToString
@Entity(name = "employee_benefit")
@Access(AccessType.FIELD)
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class EmployeeBenefit extends BaseEntity {
    private BigInteger benefitvalue;
    @ManyToOne
    private Employee employee;
    @ManyToOne
    private Benefit benefit;
    private String comments;

}
