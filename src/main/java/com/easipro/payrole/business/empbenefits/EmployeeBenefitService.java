package com.easipro.payrole.business.empbenefits;

import com.easipro.payrole.business.common.AppService;

/**
 * Created by Edward Zengeni.
 */
public interface EmployeeBenefitService extends AppService<EmployeeBenefit> {

}
