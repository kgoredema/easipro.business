package com.easipro.payrole.business.maritalstatus;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
@Service
public class MaritalStatusServiceImpl implements MaritalStatusService {

    private final MaritalStatusRepository maritalStatusRepository;


    @Override
    public Optional<MaritalStatus> get(Object id) {
        return maritalStatusRepository.findById((Long) id);
    }

    @Override
    public List<MaritalStatus> getAll() {
        return maritalStatusRepository.findAll();
    }

    @Override
    public MaritalStatus save(MaritalStatus maritalStatus) {
        return maritalStatusRepository.save(maritalStatus);
    }

    @Override
    public void delete(MaritalStatus maritalStatus) {
        maritalStatusRepository.delete(maritalStatus);

    }
}
