package com.easipro.payrole.business.maritalstatus;

import com.easipro.payrole.business.common.AppService;

/**
 * Created by Edward Zengeni.
 */
public interface MaritalStatusService extends AppService<MaritalStatus> {

}
