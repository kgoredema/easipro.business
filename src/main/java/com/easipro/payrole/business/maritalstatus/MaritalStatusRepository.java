package com.easipro.payrole.business.maritalstatus;

import org.springframework.data.jpa.repository.JpaRepository;

public interface MaritalStatusRepository extends JpaRepository<MaritalStatus, Long> {

}
