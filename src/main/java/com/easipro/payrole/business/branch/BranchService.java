package com.easipro.payrole.business.branch;

import com.easipro.payrole.business.common.AppService;

/**
 * Created by Edward Zengeni.
 */
public interface BranchService extends AppService<Branch> {

}
