package com.easipro.payrole.business.emploan;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
@Service
public class EmployeeLoanServiceImpl implements EmployeeLoanService {

    private final EmployeeLoanRepository employeeLoanRepository;


    @Override
    public Optional<EmployeeLoan> get(Object id) {
        return employeeLoanRepository.findById((Long) id);
    }

    @Override
    public List<EmployeeLoan> getAll() {
        return employeeLoanRepository.findAll();
    }

    @Override
    public EmployeeLoan save(EmployeeLoan employeeLoan) {
        return employeeLoanRepository.save(employeeLoan);
    }

    @Override
    public void delete(EmployeeLoan employeeLoan) {
        employeeLoanRepository.delete(employeeLoan);

    }
}
