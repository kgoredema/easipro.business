package com.easipro.payrole.business.emploan;

import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployeeLoanRepository extends JpaRepository<EmployeeLoan, Long> {

}
