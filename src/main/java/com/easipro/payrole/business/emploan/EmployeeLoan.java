package com.easipro.payrole.business.emploan;

import com.easipro.payrole.business.common.BaseEntity;
import com.easipro.payrole.business.employee.Employee;
import com.easipro.payrole.business.loan.Loan;
import java.math.BigInteger;
import java.util.Date;
import lombok.*;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;

@Getter
@Setter
@Builder
@ToString
@Entity(name = "employee_loan")
@Access(AccessType.FIELD)
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class EmployeeLoan extends BaseEntity {

    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dateAwarded;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date effectiveDate;
    private BigInteger loanvalue;
    private BigInteger repaymentFactor;
    @ManyToOne
    private Employee employee;
    @ManyToOne
    private Loan loan;
    private String comments;

}
