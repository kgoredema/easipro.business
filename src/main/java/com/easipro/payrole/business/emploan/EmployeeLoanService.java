package com.easipro.payrole.business.emploan;

import com.easipro.payrole.business.common.AppService;

/**
 * Created by Edward Zengeni.
 */
public interface EmployeeLoanService extends AppService<EmployeeLoan> {

}
