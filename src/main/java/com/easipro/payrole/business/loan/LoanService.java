package com.easipro.payrole.business.loan;

import com.easipro.payrole.business.common.AppService;

/**
 * Created by Edward Zengeni.
 */
public interface LoanService extends AppService<Loan> {

}
