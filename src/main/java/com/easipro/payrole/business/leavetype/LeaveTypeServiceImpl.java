package com.easipro.payrole.business.leavetype;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
@Service
public class LeaveTypeServiceImpl implements LeaveTypeService {

    private final LeaveTypeRepository leaveTypeRepository;


    @Override
    public Optional<LeaveType> get(Object id) {
        return leaveTypeRepository.findById((Long) id);
    }

    @Override
    public List<LeaveType> getAll() {
        return leaveTypeRepository.findAll();
    }

    @Override
    public LeaveType save(LeaveType leaveType) {
        return leaveTypeRepository.save(leaveType);
    }

    @Override
    public void delete(LeaveType leaveType) {
        leaveTypeRepository.delete(leaveType);

    }
}
