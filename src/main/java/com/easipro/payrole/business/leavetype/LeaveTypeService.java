package com.easipro.payrole.business.leavetype;

import com.easipro.payrole.business.common.AppService;

/**
 * Created by Edward Zengeni.
 */
public interface LeaveTypeService extends AppService<LeaveType> {

}
