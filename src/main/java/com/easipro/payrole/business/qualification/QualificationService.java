package com.easipro.payrole.business.qualification;

import com.easipro.payrole.business.common.AppService;

/**
 * Created by Edward Zengeni.
 */
public interface QualificationService extends AppService<Qualification> {

}
