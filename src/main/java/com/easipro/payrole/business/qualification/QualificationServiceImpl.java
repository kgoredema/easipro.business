package com.easipro.payrole.business.qualification;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
@Service
public class QualificationServiceImpl implements QualificationService {

    private final QualificationRepository qualificationRepository;


    @Override
    public Optional<Qualification> get(Object id) {
        return qualificationRepository.findById((Long) id);
    }

    @Override
    public List<Qualification> getAll() {
        return qualificationRepository.findAll();
    }

    @Override
    public Qualification save(Qualification qualification) {
        return qualificationRepository.save(qualification);
    }

    @Override
    public void delete(Qualification qualification) {
        qualificationRepository.delete(qualification);

    }
}
