package com.easipro.payrole.business.emprelations;

import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployeeRelationRepository extends JpaRepository<EmployeeRelation, Long> {

}
