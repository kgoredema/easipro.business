package com.easipro.payrole.business.emprelations;



import com.easipro.payrole.business.common.BaseEntity;
import com.easipro.payrole.business.employee.Employee;
import com.easipro.payrole.business.relationship.Relationship;
import lombok.*;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Getter
@Setter
@Builder
@ToString
@Entity(name = "employee_relation")
@Access(AccessType.FIELD)
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class EmployeeRelation extends BaseEntity {
    private String relativeName;
    private String comments;
    private Boolean isNextOfKin = Boolean.TRUE;
    private Boolean isBeneficiary = Boolean.TRUE;
    private Boolean isDependant = Boolean.TRUE;
    private String relativeContact;
    private String relativeAddress1;
    @ManyToOne
    private Relationship relationship;
    @ManyToOne
    private Employee employee;

}
