package com.easipro.payrole.business.emprelations;

import com.easipro.payrole.business.common.AppService;

/**
 * Created by Edward Zengeni.
 */
public interface EmployeeRelationService extends AppService<EmployeeRelation> {

}
