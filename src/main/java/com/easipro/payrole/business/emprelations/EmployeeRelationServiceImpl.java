package com.easipro.payrole.business.emprelations;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
@Service
public class EmployeeRelationServiceImpl implements EmployeeRelationService {

    private final EmployeeRelationRepository employeeRelationRepository;


    @Override
    public Optional<EmployeeRelation> get(Object id) {
        return employeeRelationRepository.findById((Long) id);
    }

    @Override
    public List<EmployeeRelation> getAll() {
        return employeeRelationRepository.findAll();
    }

    @Override
    public EmployeeRelation save(EmployeeRelation employeeRelation) {
        return employeeRelationRepository.save(employeeRelation);
    }

    @Override
    public void delete(EmployeeRelation employeeRelation) {
        employeeRelationRepository.delete(employeeRelation);

    }
}
