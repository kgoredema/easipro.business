package com.easipro.payrole.business.payslip;



import com.easipro.payrole.business.common.BaseEntity;
import com.easipro.payrole.business.deductions.Deduction;
import com.easipro.payrole.business.eanings.Earning;
import java.math.BigInteger;
import java.util.Date;
import java.util.Set;
import lombok.*;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;

@Getter
@Setter
@Builder
@ToString
@Entity(name = "payslip")
@Access(AccessType.FIELD)
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class Payslip extends BaseEntity {
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date paydate;
    private String period;
    private BigInteger basic;
    @OneToMany(mappedBy = "payslip")
    private Set<Earning> earning;
    @OneToMany(mappedBy = "payslip")
    private Set<Deduction> deduction;
    private BigInteger totalEarning;
    private BigInteger totalDeduction;
    private BigInteger netPayable;

}
