package com.easipro.payrole.business.payslip;

import com.easipro.payrole.business.common.AppService;

/**
 * Created by Edward Zengeni.
 */
public interface PayslipService extends AppService<Payslip> {

}
