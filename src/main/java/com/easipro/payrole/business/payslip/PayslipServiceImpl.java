package com.easipro.payrole.business.payslip;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
@Service
public class PayslipServiceImpl implements PayslipService {

    private final PayslipRepository payslipRepository;


    @Override
    public Optional<Payslip> get(Object id) {
        return payslipRepository.findById((Long) id);
    }

    @Override
    public List<Payslip> getAll() {
        return payslipRepository.findAll();
    }

    @Override
    public Payslip save(Payslip payslip) {
        return payslipRepository.save(payslip);
    }

    @Override
    public void delete(Payslip payslip) {
        payslipRepository.delete(payslip);

    }
}
