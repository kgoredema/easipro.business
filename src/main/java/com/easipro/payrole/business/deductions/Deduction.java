package com.easipro.payrole.business.deductions;

import com.easipro.payrole.business.common.BaseEntity;
import com.easipro.payrole.business.payslip.Payslip;
import lombok.*;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Getter
@Setter
@Builder
@ToString
@Entity(name = "deduction")
@Access(AccessType.FIELD)
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class Deduction extends BaseEntity {

    private String name;
    private String description;
    @ManyToOne
    private Payslip payslip;

}
