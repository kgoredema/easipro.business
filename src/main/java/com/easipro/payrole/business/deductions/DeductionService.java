package com.easipro.payrole.business.deductions;

import com.easipro.payrole.business.common.AppService;

/**
 * Created by Edward Zengeni.
 */
public interface DeductionService extends AppService<Deduction> {

}
