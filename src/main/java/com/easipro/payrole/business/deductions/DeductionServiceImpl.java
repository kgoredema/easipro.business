package com.easipro.payrole.business.deductions;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
@Service
public class DeductionServiceImpl implements DeductionService {

    private final DeductionRepository deductionRepository;


    @Override
    public Optional<Deduction> get(Object id) {
        return deductionRepository.findById((Long) id);
    }

    @Override
    public List<Deduction> getAll() {
        return deductionRepository.findAll();
    }

    @Override
    public Deduction save(Deduction deduction) {
        return deductionRepository.save(deduction);
    }

    @Override
    public void delete(Deduction deduction) {
        deductionRepository.delete(deduction);

    }
}
