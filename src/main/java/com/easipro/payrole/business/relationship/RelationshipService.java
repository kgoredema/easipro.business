package com.easipro.payrole.business.relationship;

import com.easipro.payrole.business.common.AppService;

/**
 * Created by Edward Zengeni.
 */
public interface RelationshipService extends AppService<Relationship> {

}
