package com.easipro.payrole.business.relationship;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
@Service
public class RelationshipServiceImpl implements RelationshipService {

    private final RelationshipRepository relationshipRepository;


    @Override
    public Optional<Relationship> get(Object id) {
        return relationshipRepository.findById((Long) id);
    }

    @Override
    public List<Relationship> getAll() {
        return relationshipRepository.findAll();
    }

    @Override
    public Relationship save(Relationship relationship) {
        return relationshipRepository.save(relationship);
    }

    @Override
    public void delete(Relationship relationship) {
        relationshipRepository.delete(relationship);

    }
}
