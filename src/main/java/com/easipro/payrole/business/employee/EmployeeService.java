package com.easipro.payrole.business.employee;

import com.easipro.payrole.business.common.AppService;

/**
 * Created by Edward Zengeni.
 */
public interface EmployeeService extends AppService<Employee> {

}
