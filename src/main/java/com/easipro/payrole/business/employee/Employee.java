package com.easipro.payrole.business.employee;

import com.easipro.payrole.business.common.BaseEntity;
import com.easipro.payrole.business.department.Department;
import com.easipro.payrole.business.empbankdetail.EmployeeBankdetail;
import com.easipro.payrole.business.empbenefits.EmployeeBenefit;
import com.easipro.payrole.business.empleave.EmployeeLeave;
import com.easipro.payrole.business.emploan.EmployeeLoan;
import com.easipro.payrole.business.emppayslip.EmployeePayslip;
import com.easipro.payrole.business.empqualification.EmployeeQualification;
import com.easipro.payrole.business.empreinstate.EmployeeReinstatement;
import com.easipro.payrole.business.emprelations.EmployeeRelation;
import com.easipro.payrole.business.emptermination.EmployeeTermination;
import com.easipro.payrole.business.gender.Gender;
import com.easipro.payrole.business.grade.Grade;
import com.easipro.payrole.business.identitytype.IdentityType;
import com.easipro.payrole.business.maritalstatus.MaritalStatus;
import com.easipro.payrole.business.payroll.Payroll;
import com.easipro.payrole.business.post.Post;
import com.easipro.payrole.business.status.EmployeeStatus;
import com.easipro.payrole.business.title.Title;
import java.util.Date;
import java.util.Set;
import lombok.*;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;

@Getter
@Setter
@Builder
@ToString
@Entity(name = "employee")
@Access(AccessType.FIELD)
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class Employee extends BaseEntity {

    private String firstname;
    private String lastname;
    private String middlename;
    @ManyToOne
    private Gender gender;
    @ManyToOne
    private Title title;
    @ManyToOne
    private MaritalStatus maritalStatus;
    @ManyToOne
    private IdentityType idType;
    private String idNumber;
    private String empNumber;
    @ManyToOne
    private Post post;
    @ManyToOne
    private Grade grade;
    @ManyToOne
    private Department department;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date appointmentDate;
    @ManyToOne
    private EmployeeStatus employeeStatus;
    @OneToMany(mappedBy = "employee")
    private Set<EmployeePayslip> employeePayslip;
    @OneToMany(mappedBy = "employee")
    private Set<EmployeeLoan> employeeLoan;
    @OneToMany(mappedBy = "employee")
    private Set<EmployeeLeave> employeeLeave;
    @OneToMany(mappedBy = "employee")
    private Set<EmployeeRelation> employeeRelation;
    @OneToMany(mappedBy = "employee")
    private Set<EmployeeQualification> qualification;
    @OneToMany(mappedBy = "employee")
    private Set<EmployeeBankdetail> employeeBankdetail;
    @OneToMany(mappedBy = "employee")
    private Set<EmployeeBenefit> employeeBenefit;
    @OneToMany(mappedBy = "employee")
    private Set<EmployeeTermination> employeeTermination;
    @OneToMany(mappedBy = "employee")
    private Set<EmployeeReinstatement> employeeReinstatement;
    @ManyToOne
    private Payroll payroll;

}
