package com.easipro.payrole.business.addresstype;

import com.easipro.payrole.business.common.AppService;

/**
 * Created by Edward Zengeni.
 */
public interface AddressTypeService extends AppService<AddressType> {

}
