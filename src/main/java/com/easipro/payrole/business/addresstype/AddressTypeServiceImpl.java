package com.easipro.payrole.business.addresstype;


import com.easipro.payrole.business.accounttype.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
@Service
public class AddressTypeServiceImpl implements AddressTypeService {

    private final AddressTypeRepository addressTypeRepository;


    @Override
    public Optional<AddressType> get(Object id) {
        return addressTypeRepository.findById((Long) id);
    }

    @Override
    public List<AddressType> getAll() {
        return addressTypeRepository.findAll();
    }

    @Override
    public AddressType save(AddressType addressType) {
        return addressTypeRepository.save(addressType);
    }

    @Override
    public void delete(AddressType addressType) {
        addressTypeRepository.delete(addressType);

    }
}
