package com.easipro.payrole.business.post;

import com.easipro.payrole.business.common.AppService;

/**
 * Created by Edward Zengeni.
 */
public interface PostService extends AppService<Post> {

}
