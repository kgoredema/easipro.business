package com.easipro.payrole.business.post;

import com.easipro.payrole.business.common.BaseEntity;
import lombok.*;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;

@Getter
@Setter
@Builder
@ToString
@Entity(name = "post")
@Access(AccessType.FIELD)
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class Post extends BaseEntity {

    private String name;
    private String description;

}
