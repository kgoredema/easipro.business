package com.easipro.payrole.business.emptermination;

import com.easipro.payrole.business.common.AppService;

/**
 * Created by Edward Zengeni.
 */
public interface EmployeeTerminationService extends AppService<EmployeeTermination> {

}
