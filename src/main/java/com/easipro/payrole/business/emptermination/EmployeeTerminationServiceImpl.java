package com.easipro.payrole.business.emptermination;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
@Service
public class EmployeeTerminationServiceImpl implements EmployeeTerminationService {

    private final EmployeeTerminationRepository employeeTerminationRepository;


    @Override
    public Optional<EmployeeTermination> get(Object id) {
        return employeeTerminationRepository.findById((Long) id);
    }

    @Override
    public List<EmployeeTermination> getAll() {
        return employeeTerminationRepository.findAll();
    }

    @Override
    public EmployeeTermination save(EmployeeTermination employeeTermination) {
        return employeeTerminationRepository.save(employeeTermination);
    }

    @Override
    public void delete(EmployeeTermination employeeTermination) {
        employeeTerminationRepository.delete(employeeTermination);

    }
}
