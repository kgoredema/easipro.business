package com.easipro.payrole.business.emptermination;

import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployeeTerminationRepository extends JpaRepository<EmployeeTermination, Long> {

}
