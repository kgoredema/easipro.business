package com.easipro.payrole.business.grade;

import com.easipro.payrole.business.common.AppService;

/**
 * Created by Edward Zengeni.
 */
public interface GradeService extends AppService<Grade> {

}
