package com.easipro.payrole.business.allowancetype;

import com.easipro.payrole.business.common.AppService;

/**
 * Created by Edward Zengeni.
 */
public interface AllowanceTypeService extends AppService<AllowanceType> {

}
