package com.easipro.payrole.business.allowancetype;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
@Service
public class AllowanceTypeServiceImpl implements AllowanceTypeService {

    private final AllowanceTypeRepository allowanceTypeRepository;


    @Override
    public Optional<AllowanceType> get(Object id) {
        return allowanceTypeRepository.findById((Long) id);
    }

    @Override
    public List<AllowanceType> getAll() {
        return allowanceTypeRepository.findAll();
    }

    @Override
    public AllowanceType save(AllowanceType allowanceType) {
        return allowanceTypeRepository.save(allowanceType);
    }

    @Override
    public void delete(AllowanceType allowanceType) {
        allowanceTypeRepository.delete(allowanceType);

    }
}
