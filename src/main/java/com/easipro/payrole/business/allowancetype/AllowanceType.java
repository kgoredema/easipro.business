package com.easipro.payrole.business.allowancetype;

import com.easipro.payrole.business.common.BaseEntity;
import lombok.*;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;

@Getter
@Setter
@Builder
@ToString
@Entity(name = "allowance_type")
@Access(AccessType.FIELD)
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class AllowanceType extends BaseEntity {

    private String name;
    private String description;

}
