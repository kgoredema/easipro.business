package com.easipro.payrole.business.allowancetype;

import org.springframework.data.jpa.repository.JpaRepository;

public interface AllowanceTypeRepository extends JpaRepository<AllowanceType, Long> {

}
