package com.easipro.payrole.business.status;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
@Service
public class EmployeeStatusServiceImpl implements EmployeeStatusService {

    private final EmployeeStatusRepository employeeStatusRepository;


    @Override
    public Optional<EmployeeStatus> get(Object id) {
        return employeeStatusRepository.findById((Long) id);
    }

    @Override
    public List<EmployeeStatus> getAll() {
        return employeeStatusRepository.findAll();
    }

    @Override
    public EmployeeStatus save(EmployeeStatus employeeStatus) {
        return employeeStatusRepository.save(employeeStatus);
    }

    @Override
    public void delete(EmployeeStatus employeeStatus) {
        employeeStatusRepository.delete(employeeStatus);

    }
}
