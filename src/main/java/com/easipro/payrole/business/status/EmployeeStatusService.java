package com.easipro.payrole.business.status;

import com.easipro.payrole.business.common.AppService;

/**
 * Created by Edward Zengeni.
 */
public interface EmployeeStatusService extends AppService<EmployeeStatus> {

}
