package com.easipro.payrole.business.empreinstate;

import com.easipro.payrole.business.common.AppService;

/**
 * Created by Edward Zengeni.
 */
public interface EmployeeReinstatementService extends AppService<EmployeeReinstatement> {

}
