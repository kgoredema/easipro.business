package com.easipro.payrole.business.empreinstate;

import com.easipro.payrole.business.common.BaseEntity;
import com.easipro.payrole.business.employee.Employee;
import java.util.Date;
import lombok.*;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import org.springframework.security.core.userdetails.User;

@Getter
@Setter
@Builder
@ToString
@Entity(name = "employee_reinstatement")
@Access(AccessType.FIELD)
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class EmployeeReinstatement extends BaseEntity {

    @ManyToOne
    private Employee employee;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date reinstatementDate;
    private String comments;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dateApproved;
    private User approvedBy;
    private Boolean approved = Boolean.FALSE;

}
