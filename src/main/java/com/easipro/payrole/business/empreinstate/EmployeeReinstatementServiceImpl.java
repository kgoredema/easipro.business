package com.easipro.payrole.business.empreinstate;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
@Service
public class EmployeeReinstatementServiceImpl implements EmployeeReinstatementService {

    private final EmployeeReinstatementRepository employeeReinstatementRepository;


    @Override
    public Optional<EmployeeReinstatement> get(Object id) {
        return employeeReinstatementRepository.findById((Long) id);
    }

    @Override
    public List<EmployeeReinstatement> getAll() {
        return employeeReinstatementRepository.findAll();
    }

    @Override
    public EmployeeReinstatement save(EmployeeReinstatement employeeReinstatement) {
        return employeeReinstatementRepository.save(employeeReinstatement);
    }

    @Override
    public void delete(EmployeeReinstatement employeeReinstatement) {
        employeeReinstatementRepository.delete(employeeReinstatement);

    }
}
