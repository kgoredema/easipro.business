package com.easipro.payrole.business.empreinstate;

import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployeeReinstatementRepository extends JpaRepository<EmployeeReinstatement, Long> {

}
