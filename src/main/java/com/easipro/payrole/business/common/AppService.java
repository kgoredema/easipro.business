package com.easipro.payrole.business.common;

import java.util.List;
import java.util.Optional;

public interface AppService<T> {

    Optional<T> get(Object id);

    List<T> getAll();

    T save(T t);

    void delete(T t);

}
