package com.easipro.payrole.business.gender;

import com.easipro.payrole.business.common.AppService;

/**
 * Created by Edward Zengeni.
 */
public interface GenderService extends AppService<Gender> {

}
