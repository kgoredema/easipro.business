package com.easipro.payrole.business.gender;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
@Service
public class GenderServiceImpl implements GenderService {

    private final GenderRepository genderRepository;


    @Override
    public Optional<Gender> get(Object id) {
        return genderRepository.findById((Long) id);
    }

    @Override
    public List<Gender> getAll() {
        return genderRepository.findAll();
    }

    @Override
    public Gender save(Gender gender) {
        return genderRepository.save(gender);
    }

    @Override
    public void delete(Gender gender) {
        genderRepository.delete(gender);

    }
}
