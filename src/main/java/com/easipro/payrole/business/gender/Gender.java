package com.easipro.payrole.business.gender;

import com.easipro.payrole.business.common.BaseEntity;
import lombok.*;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;

@Getter
@Setter
@Builder
@ToString
@Entity(name = "gender")
@Access(AccessType.FIELD)
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class Gender extends BaseEntity {

    private String name;
    private String description;

}
