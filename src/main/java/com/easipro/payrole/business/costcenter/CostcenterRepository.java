package com.easipro.payrole.business.costcenter;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CostcenterRepository extends JpaRepository<Costcenter, Long> {

}
