package com.easipro.payrole.business.costcenter;

import com.easipro.payrole.business.common.AppService;

/**
 * Created by Edward Zengeni.
 */
public interface CostcenterService extends AppService<Costcenter> {

}
