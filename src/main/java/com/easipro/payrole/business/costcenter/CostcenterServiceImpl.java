package com.easipro.payrole.business.costcenter;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
@Service
public class CostcenterServiceImpl implements CostcenterService {

    private final CostcenterRepository costcenterRepository;


    @Override
    public Optional<Costcenter> get(Object id) {
        return costcenterRepository.findById((Long) id);
    }

    @Override
    public List<Costcenter> getAll() {
        return costcenterRepository.findAll();
    }

    @Override
    public Costcenter save(Costcenter costcenter) {
        return costcenterRepository.save(costcenter);
    }

    @Override
    public void delete(Costcenter costcenter) {
        costcenterRepository.delete(costcenter);

    }
}
