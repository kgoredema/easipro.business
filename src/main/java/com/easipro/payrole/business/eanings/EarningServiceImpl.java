package com.easipro.payrole.business.eanings;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
@Service
public class EarningServiceImpl implements EarningService {

    private final EarningRepository earningRepository;


    @Override
    public Optional<Earning> get(Object id) {
        return earningRepository.findById((Long) id);
    }

    @Override
    public List<Earning> getAll() {
        return earningRepository.findAll();
    }

    @Override
    public Earning save(Earning earning) {
        return earningRepository.save(earning);
    }

    @Override
    public void delete(Earning earning) {
        earningRepository.delete(earning);

    }
}
