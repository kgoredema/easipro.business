package com.easipro.payrole.business.eanings;

import org.springframework.data.jpa.repository.JpaRepository;

public interface EarningRepository extends JpaRepository<Earning, Long> {

}
