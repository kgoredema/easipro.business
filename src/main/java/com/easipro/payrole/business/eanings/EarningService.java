package com.easipro.payrole.business.eanings;

import com.easipro.payrole.business.common.AppService;

/**
 * Created by Edward Zengeni.
 */
public interface EarningService extends AppService<Earning> {

}
