package com.easipro.payrole.business.eanings;

import com.easipro.payrole.business.common.BaseEntity;
import com.easipro.payrole.business.payslip.Payslip;
import lombok.*;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Getter
@Setter
@Builder
@ToString
@Entity(name = "earning")
@Access(AccessType.FIELD)
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class Earning extends BaseEntity {

    private String name;
    private String description;
    @ManyToOne
    private Payslip payslip;

}
