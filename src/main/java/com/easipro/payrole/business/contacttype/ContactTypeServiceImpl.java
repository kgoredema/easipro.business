package com.easipro.payrole.business.contacttype;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
@Service
public class ContactTypeServiceImpl implements ContactTypeService {

    private final ContactTypeRepository contactTypeRepository;


    @Override
    public Optional<ContactType> get(Object id) {
        return contactTypeRepository.findById((Long) id);
    }

    @Override
    public List<ContactType> getAll() {
        return contactTypeRepository.findAll();
    }

    @Override
    public ContactType save(ContactType contactType) {
        return contactTypeRepository.save(contactType);
    }

    @Override
    public void delete(ContactType contactType) {
        contactTypeRepository.delete(contactType);

    }
}
