package com.easipro.payrole.business.contacttype;

import com.easipro.payrole.business.common.AppService;

/**
 * Created by Edward Zengeni.
 */
public interface ContactTypeService extends AppService<ContactType> {

}
