package com.easipro.payrole.business.empqualification;

import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployeeQualificationRepository extends JpaRepository<EmployeeQualification, Long> {

}
