package com.easipro.payrole.business.empqualification;

import com.easipro.payrole.business.common.AppService;

/**
 * Created by Edward Zengeni.
 */
public interface EmployeeQualificationService extends AppService<EmployeeQualification> {

}
