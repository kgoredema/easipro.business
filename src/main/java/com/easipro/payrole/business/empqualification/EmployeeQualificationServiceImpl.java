package com.easipro.payrole.business.empqualification;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
@Service
public class EmployeeQualificationServiceImpl implements EmployeeQualificationService {

    private final EmployeeQualificationRepository employeeQualificationRepository;


    @Override
    public Optional<EmployeeQualification> get(Object id) {
        return employeeQualificationRepository.findById((Long) id);
    }

    @Override
    public List<EmployeeQualification> getAll() {
        return employeeQualificationRepository.findAll();
    }

    @Override
    public EmployeeQualification save(EmployeeQualification employeeQualification) {
        return employeeQualificationRepository.save(employeeQualification);
    }

    @Override
    public void delete(EmployeeQualification employeeQualification) {
        employeeQualificationRepository.delete(employeeQualification);

    }
}
