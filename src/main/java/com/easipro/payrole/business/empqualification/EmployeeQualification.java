package com.easipro.payrole.business.empqualification;

import com.easipro.payrole.business.common.BaseEntity;
import com.easipro.payrole.business.employee.Employee;
import com.easipro.payrole.business.qualification.Qualification;
import java.util.Date;
import lombok.*;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;

@Getter
@Setter
@Builder
@ToString
@Entity(name = "employee_qualification")
@Access(AccessType.FIELD)
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class EmployeeQualification extends BaseEntity {

    @Temporal(javax.persistence.TemporalType.DATE)
    private Date yearAcquired;
    @ManyToOne
    private Qualification qualification;
    @ManyToOne
    private Employee employee;

}
