package com.easipro.payrole.business.identitytype;

import org.springframework.data.jpa.repository.JpaRepository;

public interface IdentityTypeRepository extends JpaRepository<IdentityType, Long> {

}
