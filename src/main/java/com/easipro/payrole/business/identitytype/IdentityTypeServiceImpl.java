package com.easipro.payrole.business.identitytype;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
@Service
public class IdentityTypeServiceImpl implements IdentityTypeService {

    private final IdentityTypeRepository identityTypeRepository;


    @Override
    public Optional<IdentityType> get(Object id) {
        return identityTypeRepository.findById((Long) id);
    }

    @Override
    public List<IdentityType> getAll() {
        return identityTypeRepository.findAll();
    }

    @Override
    public IdentityType save(IdentityType identityType) {
        return identityTypeRepository.save(identityType);
    }

    @Override
    public void delete(IdentityType identityType) {
        identityTypeRepository.delete(identityType);

    }
}
