package com.easipro.payrole.business.identitytype;

import com.easipro.payrole.business.common.AppService;

/**
 * Created by Edward Zengeni.
 */
public interface IdentityTypeService extends AppService<IdentityType> {

}
