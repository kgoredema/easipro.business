package com.easipro.payrole.business.empleave;

import com.easipro.payrole.business.common.AppService;

/**
 * Created by Edward Zengeni.
 */
public interface EmployeeLeaveService extends AppService<EmployeeLeave> {

}
