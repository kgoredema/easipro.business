package com.easipro.payrole.business.empleave;



import com.easipro.payrole.business.common.BaseEntity;
import com.easipro.payrole.business.employee.Employee;
import com.easipro.payrole.business.leavestatus.LeaveStatus;
import com.easipro.payrole.business.leavetype.LeaveType;
import java.util.Date;
import lombok.*;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;

@Getter
@Setter
@Builder
@ToString
@Entity(name = "employee_leave")
@Access(AccessType.FIELD)
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class EmployeeLeave extends BaseEntity {
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date commenceDate;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date endDate;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date returnDate;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dateApproved;
    private String comments;
    @ManyToOne
    private LeaveType leaveType;
    @ManyToOne
    private LeaveStatus leaveStatus;
    @ManyToOne
    private Employee employee;
    private String approvedBy;

}
