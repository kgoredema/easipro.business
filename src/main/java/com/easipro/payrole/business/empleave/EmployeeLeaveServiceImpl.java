package com.easipro.payrole.business.empleave;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
@Service
public class EmployeeLeaveServiceImpl implements EmployeeLeaveService {

    private final EmployeeLeaveRepository employeeLeaveRepository;


    @Override
    public Optional<EmployeeLeave> get(Object id) {
        return employeeLeaveRepository.findById((Long) id);
    }

    @Override
    public List<EmployeeLeave> getAll() {
        return employeeLeaveRepository.findAll();
    }

    @Override
    public EmployeeLeave save(EmployeeLeave employeeLeave) {
        return employeeLeaveRepository.save(employeeLeave);
    }

    @Override
    public void delete(EmployeeLeave employeeLeave) {
        employeeLeaveRepository.delete(employeeLeave);

    }
}
