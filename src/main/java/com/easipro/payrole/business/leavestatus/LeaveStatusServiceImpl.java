package com.easipro.payrole.business.leavestatus;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
@Service
public class LeaveStatusServiceImpl implements LeaveStatusService {

    private final LeaveStatusRepository leaveStatusRepository;


    @Override
    public Optional<LeaveStatus> get(Object id) {
        return leaveStatusRepository.findById((Long) id);
    }

    @Override
    public List<LeaveStatus> getAll() {
        return leaveStatusRepository.findAll();
    }

    @Override
    public LeaveStatus save(LeaveStatus leaveStatus) {
        return leaveStatusRepository.save(leaveStatus);
    }

    @Override
    public void delete(LeaveStatus leaveStatus) {
        leaveStatusRepository.delete(leaveStatus);

    }
}
