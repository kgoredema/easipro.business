package com.easipro.payrole.business.leavestatus;

import org.springframework.data.jpa.repository.JpaRepository;

public interface LeaveStatusRepository extends JpaRepository<LeaveStatus, Long> {

}
