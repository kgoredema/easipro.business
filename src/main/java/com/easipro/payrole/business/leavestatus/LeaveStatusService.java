package com.easipro.payrole.business.leavestatus;

import com.easipro.payrole.business.common.AppService;

/**
 * Created by Edward Zengeni.
 */
public interface LeaveStatusService extends AppService<LeaveStatus> {

}
