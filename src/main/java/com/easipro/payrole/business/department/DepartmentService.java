package com.easipro.payrole.business.department;

import com.easipro.payrole.business.common.AppService;

/**
 * Created by Edward Zengeni.
 */
public interface DepartmentService extends AppService<Department> {

}
