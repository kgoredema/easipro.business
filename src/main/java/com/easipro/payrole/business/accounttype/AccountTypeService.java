package com.easipro.payrole.business.accounttype;

import com.easipro.payrole.business.common.AppService;

/**
 * Created by Edward Zengeni.
 */
public interface AccountTypeService extends AppService<AccountType> {

}
