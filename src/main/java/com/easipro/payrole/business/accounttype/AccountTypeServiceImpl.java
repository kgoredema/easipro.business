package com.easipro.payrole.business.accounttype;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
@Service
public class AccountTypeServiceImpl implements AccountTypeService {

    private final AccountTypeRepository accountTypeRepository;


    @Override
    public Optional<AccountType> get(Object id) {
        return accountTypeRepository.findById((Long) id);
    }

    @Override
    public List<AccountType> getAll() {
        return accountTypeRepository.findAll();
    }

    @Override
    public AccountType save(AccountType accountType) {
        return accountTypeRepository.save(accountType);
    }

    @Override
    public void delete(AccountType accountType) {
        accountTypeRepository.delete(accountType);

    }
}
