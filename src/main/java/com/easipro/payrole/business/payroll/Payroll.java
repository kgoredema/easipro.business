package com.easipro.payrole.business.payroll;



import com.easipro.payrole.business.common.BaseEntity;
import com.easipro.payrole.business.employee.Employee;
import java.math.BigInteger;
import java.util.Date;
import java.util.Set;
import lombok.*;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;

@Getter
@Setter
@Builder
@ToString
@Entity(name = "payroll")
@Access(AccessType.FIELD)
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class Payroll extends BaseEntity {
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date paydate;
    private String period;
    @OneToMany(mappedBy = "payroll")
    private Set<Employee> employee;
    private BigInteger total;
          

}
