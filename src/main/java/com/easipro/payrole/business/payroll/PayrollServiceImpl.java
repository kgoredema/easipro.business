package com.easipro.payrole.business.payroll;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
@Service
public class PayrollServiceImpl implements PayrollService {

    private final PayrollRepository payrollRepository;


    @Override
    public Optional<Payroll> get(Object id) {
        return payrollRepository.findById((Long) id);
    }

    @Override
    public List<Payroll> getAll() {
        return payrollRepository.findAll();
    }

    @Override
    public Payroll save(Payroll payroll) {
        return payrollRepository.save(payroll);
    }

    @Override
    public void delete(Payroll payroll) {
        payrollRepository.delete(payroll);

    }
}
