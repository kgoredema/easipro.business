package com.easipro.payrole.business.payroll;

import com.easipro.payrole.business.common.AppService;

/**
 * Created by Edward Zengeni.
 */
public interface PayrollService extends AppService<Payroll> {

}
