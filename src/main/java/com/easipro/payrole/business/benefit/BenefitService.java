package com.easipro.payrole.business.benefit;

import com.easipro.payrole.business.common.AppService;

/**
 * Created by Edward Zengeni.
 */
public interface BenefitService extends AppService<Benefit> {

}
