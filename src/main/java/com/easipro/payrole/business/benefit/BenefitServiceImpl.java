package com.easipro.payrole.business.benefit;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
@Service
public class BenefitServiceImpl implements BenefitService {

    private final BenefitRepository benefitRepository;


    @Override
    public Optional<Benefit> get(Object id) {
        return benefitRepository.findById((Long) id);
    }

    @Override
    public List<Benefit> getAll() {
        return benefitRepository.findAll();
    }

    @Override
    public Benefit save(Benefit benefit) {
        return benefitRepository.save(benefit);
    }

    @Override
    public void delete(Benefit benefit) {
        benefitRepository.delete(benefit);

    }
}
