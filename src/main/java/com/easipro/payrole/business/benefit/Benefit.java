package com.easipro.payrole.business.benefit;

import com.easipro.payrole.business.common.BaseEntity;
import lombok.*;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;

@Getter
@Setter
@Builder
@ToString
@Entity(name = "benefit")
@Access(AccessType.FIELD)
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class Benefit extends BaseEntity {

    private String name;
    private String description;

}
