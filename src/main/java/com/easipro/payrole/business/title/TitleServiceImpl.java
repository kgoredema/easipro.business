package com.easipro.payrole.business.title;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
@Service
public class TitleServiceImpl implements TitleService {

    private final TitleRepository titleRepository;


    @Override
    public Optional<Title> get(Object id) {
        return titleRepository.findById((Long) id);
    }

    @Override
    public List<Title> getAll() {
        return titleRepository.findAll();
    }

    @Override
    public Title save(Title title) {
        return titleRepository.save(title);
    }

    @Override
    public void delete(Title title) {
        titleRepository.delete(title);

    }
}
