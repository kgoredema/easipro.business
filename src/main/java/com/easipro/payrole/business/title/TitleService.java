package com.easipro.payrole.business.title;

import com.easipro.payrole.business.common.AppService;

/**
 * Created by Edward Zengeni.
 */
public interface TitleService extends AppService<Title> {

}
