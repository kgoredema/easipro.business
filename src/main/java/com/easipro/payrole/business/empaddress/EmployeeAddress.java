package com.easipro.payrole.business.empaddress;

import com.easipro.payrole.business.addresstype.AddressType;
import com.easipro.payrole.business.common.BaseEntity;
import com.easipro.payrole.business.employee.Employee;
import lombok.*;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Getter
@Setter
@Builder
@ToString
@Entity(name = "employee_address")
@Access(AccessType.FIELD)
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class EmployeeAddress extends BaseEntity {

    private String address1;
    private String address2;
    private String city;
    private boolean status = true;
    @ManyToOne
    private AddressType addressType;
    @ManyToOne
    private Employee employee;

}
