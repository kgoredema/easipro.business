package com.easipro.payrole.business.empaddress;

import com.easipro.payrole.business.common.AppService;

/**
 * Created by Edward Zengeni.
 */
public interface EmployeeAddressService extends AppService<EmployeeAddress> {

}
