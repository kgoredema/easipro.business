package com.easipro.payrole.business.empaddress;

import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployeeAddressRepository extends JpaRepository<EmployeeAddress, Long> {

}
