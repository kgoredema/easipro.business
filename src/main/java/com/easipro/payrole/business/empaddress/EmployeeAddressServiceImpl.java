package com.easipro.payrole.business.empaddress;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
@Service
public class EmployeeAddressServiceImpl implements EmployeeAddressService {

    private final EmployeeAddressRepository employeeAddressRepository;


    @Override
    public Optional<EmployeeAddress> get(Object id) {
        return employeeAddressRepository.findById((Long) id);
    }

    @Override
    public List<EmployeeAddress> getAll() {
        return employeeAddressRepository.findAll();
    }

    @Override
    public EmployeeAddress save(EmployeeAddress employeeAddress) {
        return employeeAddressRepository.save(employeeAddress);
    }

    @Override
    public void delete(EmployeeAddress employeeAddress) {
        employeeAddressRepository.delete(employeeAddress);

    }
}
