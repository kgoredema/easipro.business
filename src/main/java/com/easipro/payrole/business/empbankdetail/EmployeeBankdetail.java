package com.easipro.payrole.business.empbankdetail;



import com.easipro.payrole.business.accounttype.AccountType;
import com.easipro.payrole.business.common.BaseEntity;
import com.easipro.payrole.business.employee.Employee;
import lombok.*;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Getter
@Setter
@Builder
@ToString
@Entity(name = "employee_bankdetail")
@Access(AccessType.FIELD)
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class EmployeeBankdetail extends BaseEntity {
   private String accountno;
    private String accountName;
    private String branch;
    private String bank;
    @ManyToOne
    private Employee employee;
    @ManyToOne
    private AccountType accountType;
    private Boolean status = Boolean.TRUE;

}
