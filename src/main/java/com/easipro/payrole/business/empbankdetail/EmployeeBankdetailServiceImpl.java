package com.easipro.payrole.business.empbankdetail;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
@Service
public class EmployeeBankdetailServiceImpl implements EmployeeBankdetailService {

    private final EmployeeBankdetailRepository employeeBankdetailRepository;


    @Override
    public Optional<EmployeeBankdetail> get(Object id) {
        return employeeBankdetailRepository.findById((Long) id);
    }

    @Override
    public List<EmployeeBankdetail> getAll() {
        return employeeBankdetailRepository.findAll();
    }

    @Override
    public EmployeeBankdetail save(EmployeeBankdetail employeeBankdetail) {
        return employeeBankdetailRepository.save(employeeBankdetail);
    }

    @Override
    public void delete(EmployeeBankdetail employeeBankdetail) {
        employeeBankdetailRepository.delete(employeeBankdetail);

    }
}
