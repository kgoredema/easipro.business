package com.easipro.payrole.business.empbankdetail;

import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployeeBankdetailRepository extends JpaRepository<EmployeeBankdetail, Long> {

}
