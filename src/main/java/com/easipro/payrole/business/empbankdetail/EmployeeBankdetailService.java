package com.easipro.payrole.business.empbankdetail;

import com.easipro.payrole.business.common.AppService;

/**
 * Created by Edward Zengeni.
 */
public interface EmployeeBankdetailService extends AppService<EmployeeBankdetail> {

}
