package com.easipro.payrole.business.empcontact;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
@Service
public class EmployeeContactServiceImpl implements EmployeeContactService {

    private final EmployeeContactRepository employeeContactRepository;


    @Override
    public Optional<EmployeeContact> get(Object id) {
        return employeeContactRepository.findById((Long) id);
    }

    @Override
    public List<EmployeeContact> getAll() {
        return employeeContactRepository.findAll();
    }

    @Override
    public EmployeeContact save(EmployeeContact employeeContact) {
        return employeeContactRepository.save(employeeContact);
    }

    @Override
    public void delete(EmployeeContact employeeContact) {
        employeeContactRepository.delete(employeeContact);

    }
}
