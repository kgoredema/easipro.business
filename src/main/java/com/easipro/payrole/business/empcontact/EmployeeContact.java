package com.easipro.payrole.business.empcontact;



import com.easipro.payrole.business.common.BaseEntity;
import com.easipro.payrole.business.contacttype.ContactType;
import com.easipro.payrole.business.employee.Employee;
import lombok.*;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Getter
@Setter
@Builder
@ToString
@Entity(name = "employee_contact")
@Access(AccessType.FIELD)
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class EmployeeContact extends BaseEntity {
    private String contactDetail;
    private boolean status = true;
    @ManyToOne
    private ContactType contactType;
    @ManyToOne
    private Employee employee;

}
