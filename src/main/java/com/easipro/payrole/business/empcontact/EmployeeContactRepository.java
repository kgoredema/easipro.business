package com.easipro.payrole.business.empcontact;

import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployeeContactRepository extends JpaRepository<EmployeeContact, Long> {

}
