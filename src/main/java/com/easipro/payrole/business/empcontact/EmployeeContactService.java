package com.easipro.payrole.business.empcontact;

import com.easipro.payrole.business.common.AppService;

/**
 * Created by Edward Zengeni.
 */
public interface EmployeeContactService extends AppService<EmployeeContact> {

}
