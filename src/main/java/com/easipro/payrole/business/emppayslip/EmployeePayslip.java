package com.easipro.payrole.business.emppayslip;



import com.easipro.payrole.business.common.BaseEntity;
import com.easipro.payrole.business.employee.Employee;
import com.easipro.payrole.business.payslip.Payslip;
import lombok.*;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Getter
@Setter
@Builder
@ToString
@Entity(name = "employee_payslip")
@Access(AccessType.FIELD)
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class EmployeePayslip extends BaseEntity {
    @ManyToOne
    private Payslip payslip;
    @ManyToOne
    private Employee employee;

}
