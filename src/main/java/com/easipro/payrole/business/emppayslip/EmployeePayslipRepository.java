package com.easipro.payrole.business.emppayslip;

import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployeePayslipRepository extends JpaRepository<EmployeePayslip, Long> {

}
