package com.easipro.payrole.business.emppayslip;

import com.easipro.payrole.business.common.AppService;

/**
 * Created by Edward Zengeni.
 */
public interface EmployeePayslipService extends AppService<EmployeePayslip> {

}
