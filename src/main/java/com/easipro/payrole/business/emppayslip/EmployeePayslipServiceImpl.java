package com.easipro.payrole.business.emppayslip;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
@Service
public class EmployeePayslipServiceImpl implements EmployeePayslipService {

    private final EmployeePayslipRepository employeePayslipRepository;


    @Override
    public Optional<EmployeePayslip> get(Object id) {
        return employeePayslipRepository.findById((Long) id);
    }

    @Override
    public List<EmployeePayslip> getAll() {
        return employeePayslipRepository.findAll();
    }

    @Override
    public EmployeePayslip save(EmployeePayslip employeePayslip) {
        return employeePayslipRepository.save(employeePayslip);
    }

    @Override
    public void delete(EmployeePayslip employeePayslip) {
        employeePayslipRepository.delete(employeePayslip);

    }
}
